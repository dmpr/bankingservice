package com.dmpr.demo.monese.bankingservice;

import com.dmpr.demo.monese.bankingservice.domain.BalanceDto;
import com.dmpr.demo.monese.bankingservice.domain.MoneyTransferDto;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

import javax.servlet.ServletContext;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestMethodOrder(MethodOrderer.MethodName.class)
class BankingServiceApplicationTests {

	private static String ALICE_ACCOUNT_NAME = "ALICE_ACCOUNT_1";
	private static String BOB_ACCOUNT_NAME = "BOB_ACCOUNT_1";
	private static final String CONTENT_TYPE = "application/json";

	@Autowired
	private WebApplicationContext webApplicationContext;

	private static HttpHeaders headers;

	private MockMvc mockMvc;

	@BeforeEach
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
	}

	@Test
	void contextLoads() {
		ServletContext servletContext = webApplicationContext.getServletContext();

		assertNotNull(servletContext);
		assertTrue(servletContext instanceof MockServletContext);
		assertNotNull(webApplicationContext.getBean("accountController"));
		assertNotNull(webApplicationContext.getBean("transferFundsController"));
	}


	@Test
	public void checkBalance() throws Exception {
		BalanceDto aliceBalance = new BalanceDto(ALICE_ACCOUNT_NAME, new BigDecimal(1000), Currency.getInstance("EUR"));

		final MvcResult mvcResult = mockMvc
				.perform(
						get("/api/accounts/balance/{ACCOUNT_NAME}", ALICE_ACCOUNT_NAME))
				.andExpect(
						status().isOk())
				.andExpect(
						jsonPath("$.accountRef").value(aliceBalance.getAccountRef()))
				.andExpect(
						jsonPath("$.balance").value(aliceBalance.getBalance().setScale(2, RoundingMode.HALF_UP)))
				.andExpect(
						jsonPath("$.currency").value(aliceBalance.getCurrency().getCurrencyCode()))
				.andReturn();

		assertEquals(CONTENT_TYPE, mvcResult.getResponse().getContentType());
	}

	@Test
	public void transferFundsAndCheckStatement() throws Exception {
		MoneyTransferDto mtDto = new MoneyTransferDto("tr1", "ALICE_ACCOUNT_1", "BOB_ACCOUNT_1", new BigDecimal("100"), Currency.getInstance("EUR"));

		JSONObject moneyTransferJson = new JSONObject();
		moneyTransferJson.put("transactionRef", mtDto.getTransactionRef());
		moneyTransferJson.put("senderAccountRef", mtDto.getSenderAccountRef());
		moneyTransferJson.put("receiverAccountRef", mtDto.getReceiverAccountRef());
		moneyTransferJson.put("moneyAmount", mtDto.getMoneyAmount());
		moneyTransferJson.put("currency", mtDto.getCurrency());

		final MvcResult mvcResult = mockMvc
				.perform(
						post("/api/accounts/transferfunds")
						.contentType(
							MediaType.APPLICATION_JSON)
						.content(
							moneyTransferJson.toString()))
				.andExpect(status().is2xxSuccessful())
				.andReturn();

		assertEquals(CONTENT_TYPE, mvcResult.getResponse().getContentType());

		final MvcResult mvcResult2 = mockMvc
				.perform(
						get("/api/accounts/statement/{ACCOUNT_NAME}", BOB_ACCOUNT_NAME))
				.andExpect(
						status().isOk())
				.andExpect(
						jsonPath("$.transactions", hasSize(1)))
				.andExpect(
						jsonPath("$.transactions[0].transactionRef").value(mtDto.getTransactionRef()))
				.andExpect(
						jsonPath("$.transactions[0].counterpartyAccountRef").value(mtDto.getSenderAccountRef()))
				.andExpect(
						jsonPath("$.transactions[0].moneyAmount").value(mtDto.getMoneyAmount().setScale(2, RoundingMode.HALF_UP)))
				.andExpect(
						jsonPath("$.transactions[0].currency").value(mtDto.getCurrency().getCurrencyCode()))
				.andReturn();

		assertEquals(CONTENT_TYPE, mvcResult.getResponse().getContentType());
	}

	@Test
	public void checkAccountDetailedStatement() throws Exception {
		// TODO: (dmpr) implement test

		final MvcResult mvcResult = mockMvc
				.perform(
						get("/api/accounts/detailed-statement/{ALICE_ACCOUNT_NAME}", ALICE_ACCOUNT_NAME))
				.andExpect(
						status().isOk())
				.andReturn();

		assertEquals(CONTENT_TYPE, mvcResult.getResponse().getContentType());
	}

}