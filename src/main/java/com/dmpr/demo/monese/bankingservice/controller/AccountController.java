package com.dmpr.demo.monese.bankingservice.controller;

import com.dmpr.demo.monese.bankingservice.domain.BalanceDto;
import com.dmpr.demo.monese.bankingservice.service.AccountService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {
    private static final Logger logger = Logger.getLogger(AccountController.class);

    @Autowired
    AccountService accountService;

    @RequestMapping(value = "/balance/{accountRef}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<BalanceDto> getBalance(@PathVariable String accountRef) {

        BalanceDto balance = accountService.getBalance(accountRef);
        logger.info("Balance for account "  + balance.getAccountRef() + " is " + balance.getBalance() + " " + balance.getCurrency());

        return  ResponseEntity.ok(balance);
    }
}
