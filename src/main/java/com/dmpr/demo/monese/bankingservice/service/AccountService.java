package com.dmpr.demo.monese.bankingservice.service;

import com.dmpr.demo.monese.bankingservice.domain.BalanceDto;
import com.yanimetaxas.bookkeeping.Ledger;
import com.yanimetaxas.bookkeeping.model.Money;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    @Autowired
    Ledger ledger;

    public BalanceDto getBalance(String accountRef) {

        Money moneyOnTheAccount = ledger.getAccountBalance(accountRef);
        BalanceDto currentBalance = new BalanceDto(accountRef, moneyOnTheAccount.getAmount(), moneyOnTheAccount.getCurrency());

        return  currentBalance;
    }
}
