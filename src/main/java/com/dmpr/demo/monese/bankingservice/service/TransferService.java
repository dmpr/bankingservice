package com.dmpr.demo.monese.bankingservice.service;

import com.dmpr.demo.monese.bankingservice.domain.StatementDto;
import com.dmpr.demo.monese.bankingservice.domain.TransactionDto;
import com.yanimetaxas.bookkeeping.Ledger;
import com.yanimetaxas.bookkeeping.exception.BusinessException;
import com.yanimetaxas.bookkeeping.model.Money;
import com.yanimetaxas.bookkeeping.model.Transaction;
import com.yanimetaxas.bookkeeping.model.TransactionLeg;
import com.yanimetaxas.bookkeeping.model.TransferRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
public class TransferService {

    @Autowired
    Ledger ledger;

    public void transferFunds(String transactionRef, String sourceAccountRef, String targetAccountRef, Money amount) throws BusinessException {
        String currencyCode = amount.getCurrency().getCurrencyCode();
        BigDecimal amountValue = amount.getAmount();

        TransferRequest transferRequest1 = ledger.createTransferRequest()
                .reference(transactionRef)
                .type("testing1")
                .account(sourceAccountRef).debit(amountValue.toString(), currencyCode)
                .account(targetAccountRef).credit(amountValue.toString(), currencyCode)
                .build();

        ledger.commit(transferRequest1);
    }

    public List<Transaction> getAllTransactions(String accountRef) {
        List<Transaction> transactionsByAccountRef = ledger.getTransferService().findTransactionsByAccountRef(accountRef);

        return transactionsByAccountRef;
    }

    public StatementDto getStatement(String accountRef) {
        List<Transaction> transactionsByAccountRef = ledger.getTransferService().findTransactionsByAccountRef(accountRef);


        List<TransactionDto> transactions = transactionsByAccountRef
                .stream()
                .map(transaction -> simplisticallyConvertToTransactionDto(transaction, accountRef))
                .collect(toList());

        return new StatementDto(accountRef, transactions);
    }

    // dmpr: just for demo purposes
    private TransactionDto simplisticallyConvertToTransactionDto(Transaction transaction, String accountRef) {
        List<TransactionLeg> legsForAccount = transaction.getLegs().stream()
                .filter(leg -> !leg.getAccountRef().equals(accountRef))
                .collect(toList());

        Optional<TransactionLeg> first = legsForAccount.stream().findFirst();
        if (first.isPresent()) {
            TransactionLeg transactionLeg = first.get();
            return new TransactionDto(transaction.getTransactionRef(),
                    transactionLeg.getAccountRef(),
                    transactionLeg.getAmount().getAmount().negate(),
                    transactionLeg.getAmount().getCurrency());
        } else {
            return new TransactionDto();
        }
    }
}
