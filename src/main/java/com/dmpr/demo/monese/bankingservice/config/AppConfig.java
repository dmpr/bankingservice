package com.dmpr.demo.monese.bankingservice.config;

import com.yanimetaxas.bookkeeping.ChartOfAccounts;
import com.yanimetaxas.bookkeeping.Ledger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static com.yanimetaxas.bookkeeping.ConnectionOptions.EMBEDDED_H2_CONNECTION;


@EnableWebMvc
@Configuration
@ComponentScan(basePackages = { "com.dmpr.demo.controller", "com.dmpr.demo.service" })
public class AppConfig {

    @Bean
    @Scope("singleton")
    public ChartOfAccounts chartOfAccounts() {
        ChartOfAccounts chartOfAccounts = new ChartOfAccounts.ChartOfAccountsBuilder()
                .create("ALICE_ACCOUNT_1", "1000.00", "EUR")
                .create("BOB_ACCOUNT_1", "0.00", "EUR")
                .create("CHUCK_ACCOUNT_1", "10000000.01", "EUR")
                .build();

        return chartOfAccounts;
    }

    @Bean
    @Scope("singleton")
    public Ledger ledger() {
        Ledger ledger = new Ledger.LedgerBuilder(chartOfAccounts())
                .name("Monese banking service embedded H2 ledger")
                .options(EMBEDDED_H2_CONNECTION)
                .build()
                .init();

        return ledger;
    }
}