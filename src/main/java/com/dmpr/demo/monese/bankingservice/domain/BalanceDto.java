package com.dmpr.demo.monese.bankingservice.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Currency;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BalanceDto {
    String accountRef;
    @JsonFormat(shape=JsonFormat.Shape.STRING)
    BigDecimal balance;
    Currency currency;
}