package com.dmpr.demo.monese.bankingservice.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Currency;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TransactionDto {

    String transactionRef;
    String counterpartyAccountRef;
    @JsonFormat(shape=JsonFormat.Shape.STRING)
    BigDecimal moneyAmount;
    Currency currency;

}
