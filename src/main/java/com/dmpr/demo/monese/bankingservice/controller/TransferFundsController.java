package com.dmpr.demo.monese.bankingservice.controller;

import com.dmpr.demo.monese.bankingservice.domain.MoneyTransferDto;
import com.dmpr.demo.monese.bankingservice.domain.StatementDto;
import com.dmpr.demo.monese.bankingservice.service.TransferService;
import com.yanimetaxas.bookkeeping.model.Money;
import com.yanimetaxas.bookkeeping.model.Transaction;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.RoundingMode;
import java.util.List;

@RestController
@RequestMapping("/api/accounts")
public class TransferFundsController {

    private static final Logger logger = Logger.getLogger(TransferFundsController.class);

    @Autowired
    TransferService transferService;

    @PostMapping(value = "/transferfunds", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> transferFunds(@RequestBody MoneyTransferDto moneyTransfer) {

        String transactionRef = moneyTransfer.getTransactionRef();
        String senderAccountRef = moneyTransfer.getSenderAccountRef();
        String receiverAccountRef = moneyTransfer.getReceiverAccountRef();
        Money money = new Money(moneyTransfer.getMoneyAmount().setScale(2, RoundingMode.HALF_UP), moneyTransfer.getCurrency());

        transferService.transferFunds(transactionRef, senderAccountRef, receiverAccountRef, money);

        logger.log(Level.DEBUG, "Funds transfer complete");

        // TODO: (dmpr) Implement proper error handling (com.yanimetaxas.bookkeeping.exception.BusinessException) and return correct HTTP codes (according to design.txt)
        return ResponseEntity.ok("");
    }

    @RequestMapping(value = "/statement/{accountRef}", method = RequestMethod.GET)
    public ResponseEntity<StatementDto> getStatement(@PathVariable String accountRef) {

        StatementDto statements = transferService.getStatement(accountRef);

        return ResponseEntity.ok(statements);
    }

    @RequestMapping(value = "/detailed-statement/{accountRef}", method = RequestMethod.GET)
    public ResponseEntity<List<Transaction>> getDetailedStatement(@PathVariable String accountRef) {
        List<Transaction> statement = transferService.getAllTransactions(accountRef);
        return ResponseEntity.ok(statement);
    }
}
